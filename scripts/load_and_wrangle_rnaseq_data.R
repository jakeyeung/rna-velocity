# Jake Yeung
# Date of Creation: 2017-11-18
# File: ~/projects/rna-velocity/scripts/load_and_wrangle_rnaseq_data.R
# Run on Atger data

rm(list=ls())

# # install required packages
# install.packages("devtools")
# source("https://bioconductor.org/biocLite.R")
# biocLite("pcaMethods")
# 
# library(devtools)
# install_github("velocyto-team/velocyto.R")

# install_github("naef-lab/PhaseHSV")

# Load libraries ----------------------------------------------------------

library(velocyto.R)
library(dplyr)
library(ggplot2)
library(PhaseHSV)

GetClockGenes <- function(){
  # return clock genes
  clockgenes <- c('Nr1d1','Dbp', 'Arntl', 'Npas2', 'Nr1d2',
                  'Bhlhe41', 'Nfil3', 'Cdkn1a', 'Lonrf3',
                  'Tef', 'Usp2', 'Wee1', 'Dtx4',
                  'Clock', 'Per1', 'Per2', 'Per3', 'Cry2', 'Cry1')
  return(clockgenes)
}

RotatePhase <- Vectorize(function(phase, rotate.hr=0){
  # rotate phase, in hours. If negative add 24. If > 24 minus 24.
  if (is.na(phase)) return(phase)
  phase <- phase + rotate.hr
  if (phase < 0) phase <- phase + 24
  if (phase > 24) phase <- phase - 24
  return(phase)
}, "phase")

GetRegion <- function(jstr){
  strsplit(jstr, "_")[[1]][[1]]
}

MakeLong <- function(dat){
  dat.sub <- dat[, grepl("Gene_Symbol|^Intron|^Exon", colnames(dat))]
  cnames <- colnames(dat.sub)[!grepl("Gene_Symbol", colnames(dat.sub))]
  regions <- sapply(cnames, GetRegion, USE.NAMES = FALSE)
}

# Load data ---------------------------------------------------------------

install.packages("xlsx")
library(xlsx)

inf2 <- "/home/shared/rnavelocity_data/Gotic/downshift.txt"
dat <- read.table(inf2, header = TRUE)

inf.xls <- "/home/shared/liver-zonation/data_tables/Gotic/downshift.xls"
dat.xls <- read.xlsx2(inf.xls, sheet = 1)

inf <- "/home/shared/liver-zonation/data_tables/atger_rf_wt_ko_FromPaper.txt"

dat <- read.table(inf, header = TRUE, stringsAsFactors = TRUE)

dat.long <- MakeLong(dat)
# dat <- subset(dat, model == 12)  # Dbp

# emat <- dat[, grepl("^Exon|^Gene_Symbol$", colnames(dat))]
emat <- dat[, grepl("^Exon", colnames(dat))]

emat <- sweep(emat, MARGIN = 1, STATS = rowMeans(emat), FUN = "-")

rownames(emat) <- make.names(dat$Gene_Symbol, unique = TRUE)
colnames(emat) <- gsub("Exon_", "", colnames(emat))
# nmat <- dat[, grepl("^Intron|^Gene_Symbol$", colnames(dat))]
nmat <- dat[, grepl("^Intron", colnames(dat))]
nmat <- t(scale(t(nmat), center = TRUE, scale = FALSE))
colnames(nmat) <- gsub("Intron_", "", colnames(nmat))
rownames(nmat) <- make.names(dat$Gene_Symbol, unique = TRUE)

nmat <- sweep(nmat, MARGIN = 1, STATS = rowMeans(nmat), FUN = "-")

rvel.qf <- gene.relative.velocity.estimates(2^emat,2^nmat,deltaT=1,kCells = 3,fit.quantile = 0.02)
# rvel.qf <- gene.relative.velocity.estimates(emat,nmat,deltaT=1,kCells = 1,fit.quantile = 0.02)

# Density of Gammas
plot(density(rvel.qf$gamma))



phases <- as.numeric(gsub("[^\\d]+", "", colnames(emat), perl=TRUE))
phase.hue <- PhaseToHsv(RotatePhase(phases, rotate.hr = -8), min.phase = 0, max.phase = 24)
cell.colors <- hsv(phase.hue, 1, 1)

names(cell.colors) <- colnames(emat)
pca.velocity.plot(rvel.qf,
                  scale = "log",
                  nPcs=2,
                  plot.cols=1,
                  cell.colors=ac(cell.colors, alpha = 0.3), 
                  cex=4,pcount=0.1,pc.multipliers=c(-1, 1))


# Do cells relax to a line ?  ---------------------------------------------



# do normal pca -----------------------------------------------------------

pca.out <- prcomp(emat, center = FALSE, scale. = FALSE)

plot(pca.out$rotation[, 1], pca.out$rotation[, 2])
